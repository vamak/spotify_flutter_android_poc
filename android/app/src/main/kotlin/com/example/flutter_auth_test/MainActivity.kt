package com.example.flutter_auth_test

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProviders
import com.spotify.sdk.android.auth.AuthorizationClient
import com.spotify.sdk.android.auth.AuthorizationRequest
import com.spotify.sdk.android.auth.AuthorizationResponse
import io.flutter.app.FlutterFragmentActivity
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel

/*
 */

class MainActivity : FlutterFragmentActivity() {
    companion object {
        const val CHANNEL = "flutter.native/helper"

        const val CLIENT_ID = "6808e9c95d354e2290cc6385a905075a"
        const val AUTH_TOKEN_REQUEST_CODE = 0x10

        fun getAuthenticationRequest(type: AuthorizationResponse.Type) =
                AuthorizationRequest.Builder(CLIENT_ID, type, getRedirectUri().toString())
                        .setShowDialog(false)
                        .setScopes(arrayOf("user-read-playback-state", "user-read-email"))
                        .setCustomParam("customParam1", "customValue1")
                        .build()

        fun getRedirectUri(): Uri {
            return Uri.Builder()
                    .scheme("spotify-sdk")
                    .authority("auth")
                    .build()
        }
    }

    lateinit var viewModel: MainActivityViewModel

    inner class AuthenticationCallHander: MethodChannel.MethodCallHandler {
        override fun onMethodCall(call: MethodCall, result: MethodChannel.Result) {
            if (call.method == "startAuthentication") {
                // do something with viewModel
                viewModel.authenticationContext = AuthenticationContext(call, result)
                startAuthentication()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val channel = MethodChannel(flutterView, CHANNEL).apply {
            setMethodCallHandler(AuthenticationCallHander())
        }
        viewModel = ViewModelProviders.of(this, MainActivityViewModelFactory(channel))
                .get(MainActivityViewModel::class.java)
    }

    fun startAuthentication() {
        val request = getAuthenticationRequest(AuthorizationResponse.Type.CODE)
        AuthorizationClient.openLoginActivity(this, AUTH_TOKEN_REQUEST_CODE, request)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val response = AuthorizationClient.getResponse(resultCode, data)
        if (response.error != null && response.error.isEmpty()) {
            Log.e("MainActivity", "Failure: ${response.error}")
        } else if (requestCode == AUTH_TOKEN_REQUEST_CODE) {
            Log.i("MainActivity", "Got access code ${response.code}")
            viewModel.authenticationContext!!.result.success(response.code)
            viewModel.authenticationContext = null
//            mAccessToken = response.accessToken
        }
    }
}
