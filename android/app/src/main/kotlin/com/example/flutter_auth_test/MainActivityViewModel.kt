package com.example.flutter_auth_test

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel

data class AuthenticationContext(val methodCall: MethodCall, val result: MethodChannel.Result)

class MainActivityViewModel(val channel: MethodChannel): ViewModel() {
    var authenticationContext: AuthenticationContext? = null
}

class MainActivityViewModelFactory(val channel: MethodChannel): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = when {
        modelClass.isAssignableFrom(MainActivityViewModel::class.java) -> MainActivityViewModel(channel) as T
        else -> throw IllegalArgumentException("ViewModel $modelClass not known")
    }
}