import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Spotify auth flow!'),
    );
  }
}

const _clientId = "6808e9c95d354e2290cc6385a905075a";
const _clientSecret = "1f7f69e11dd040aaae0fb8553b982d5c";
const _redirectUri = "spotify-sdk://auth";

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String accessToken = "";
  String accessCode = "";
  String refreshToken = "";
  String playbackState = "";
  static const platform = const MethodChannel('flutter.native/helper');
  final httpClient = HttpClient();

  @override
  void initState() {
    super.initState();
  }

  void _startAuth() async {
    try {
      final String result = await platform.invokeMethod('startAuthentication');
      print("!!! accessCode=$result");
      setState(() {
        accessCode = result;
      });
    } on PlatformException catch (e) {
      setState(() {
        accessCode = "Failed to Invoke: '${e.message}'.";
      });
    }
  }

  void _requestAccessAndRefreshTokens() async {
    final uri = Uri(
      scheme: "https",
      host: "accounts.spotify.com",
      path: "/api/token",
      queryParameters: {
        "grant_type": "authorization_code",
        "code": accessCode,
        "redirect_uri": _redirectUri,
        "client_id": _clientId,
        "client_secret": _clientSecret,
      },
    );

    final response = await httpClient
        .postUrl(uri)
        .then((request) {
          request.headers
              .add("Content-Type", "application/x-www-form-urlencoded");
          return request.close();
        })
        .then((value) => value.transform(utf8.decoder).toList())
        .then((value) => value.fold(
            "", (previousValue, element) => "$previousValue$element"))
        .then((value) => json.decode(value) as Map<String, dynamic>)
        .catchError((error) {
          print("Something went wrong: $error");
          return const <String, dynamic>{};
        });

    setState(() {
      accessToken = response["access_token"] as String ?? "smth went wrong";
      refreshToken = response["refresh_token"] as String ?? "smth went wrong";
    });
  }

  String encodeClientIdAndSecret() {
    return "";
  }

  void _refreshAccessToken() async {
    final uri = Uri(
      scheme: "https",
      host: "accounts.spotify.com",
      path: "/api/token",
      queryParameters: {
        "grant_type": "refresh_token",
        "refresh_token": refreshToken,
        "client_id": _clientId,
        "client_secret": _clientSecret,
      },
    );

    final response = await httpClient
        .postUrl(uri)
        .then((request) {
          request.headers
              .add("Content-Type", "application/x-www-form-urlencoded");
          return request.close();
        })
        .then((value) => value.transform(utf8.decoder).toList())
        .then((value) => value.fold(
            "", (previousValue, element) => "$previousValue$element"))
        .then((value) => json.decode(value) as Map<String, dynamic>)
        .catchError((error) {
          print("Something went wrong: $error");
          return const <String, dynamic>{};
        });

    setState(() {
      accessToken = response["access_token"] as String ?? "smth went wrong";
      print("!!! set the accessToken to '$accessToken'");
    });
  }

  void _getPlaybackState() async {
    final uri = Uri(
      scheme: "https",
      host: "api.spotify.com",
      path: "/v1/me/player",
    );

    final response = await httpClient
        .getUrl(uri)
        .then((request) {
          request.headers
              ..add("Accept", "application/json")
              ..add("Content-Type", "application/json")
              ..add("Authorization", "Bearer $accessToken");
          return request.close();
        })
        .then((value) => value.transform(utf8.decoder).toList())
        .then((value) => value.fold(
            "", (previousValue, element) => "$previousValue$element"))
        .then((value) => json.decode(value) as Map<String, dynamic>)
        .catchError((error) {
          return const <String, dynamic>{};
        });

    setState(() {
      if (response.isEmpty) {
        playbackState = "Nothing is playing";
      } else {
        final isPlaying = response["is_playing"] ?? "null";
        final item = response["item"] as Map<String, dynamic> ?? {};
        final trackName = item["name"] ?? "null";
        final firstArtist = (item["artists"] as List<dynamic>).first as Map<String, dynamic>;
        final artistName = firstArtist["name"] ?? "null";

        playbackState = "Playing = $isPlaying; Track = $trackName; Artist = $artistName";
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Text(
                "Access code: $accessCode",
                textScaleFactor: 1.3,
              ),
              RaisedButton(
                child: Text(
                  "Authenticate",
                  textScaleFactor: 2,
                ),
                onPressed: _startAuth,
              ),
              SizedBox(
                height: 50,
              ),
              Text(
                "Access token: $accessToken",
                textScaleFactor: 1.3,
              ),
              Text(
                "Refresh token: $refreshToken",
                textScaleFactor: 1.3,
              ),
              RaisedButton(
                child: Text(
                  "Request tokens",
                  textScaleFactor: 2,
                ),
                onPressed: (accessCode.isNotEmpty)
                    ? _requestAccessAndRefreshTokens
                    : null,
              ),
              SizedBox(
                height: 50,
              ),
              RaisedButton(
                child: const Text(
                  "Refresh access tokens",
                  textScaleFactor: 2,
                ),
                onPressed: (accessToken.isNotEmpty && refreshToken.isNotEmpty)
                    ? _refreshAccessToken
                    : null,
              ),
              SizedBox(
                height: 50,
              ),
              Text(
                playbackState,
                textScaleFactor: 1.5,
              ),
              RaisedButton(
                child: Text(
                  "Get Playback state",
                  textScaleFactor: 2,
                ),
                onPressed: (accessToken.isNotEmpty) ? _getPlaybackState : null,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
